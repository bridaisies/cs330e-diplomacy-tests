# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    def test_read(self):
        s = "A Madrid Hold\n"
        a = diplomacy_read(s)
        self.assertEqual(a,["A","Madrid","Hold"])


    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        a = diplomacy_read(s)
        self.assertEqual(a, ["B", "Barcelona", "Move", "Madrid"])


    def test_read_3(self):
        s = "C London Support B\n"
        a = diplomacy_read(s)
        self.assertEqual(a, ["C", "London", "Support", "B"])


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([["A","Madrid","Hold"],["B", "Barcelona", "Move", "Madrid"],["C", "London", "Support", "B"]])
        self.assertEqual(v, [["A","[dead]"],["B", "Madrid"],["C","London"]])

    def test_eval_2(self):
        v = diplomacy_eval([["A","Madrid","Hold"], ["B", "Barcelona", "Move", "Madrid"]])
        self.assertEqual(v, [["A","[dead]"],["B","[dead]"]])

    def test_eval_3(self):
        v = diplomacy_eval([["A","Madrid","Hold"], ["B", "Barcelona", "Move", "Madrid"],["C", "London", "Support", "B"],["D", "Austin", "Move", "London"]])
        self.assertEqual(v, [["A","[dead]"],["B","[dead]"],["C","[dead]"],["D","[dead]"]])

    def test_eval_4(self):
        v = diplomacy_eval([["A","Madrid","Hold"],["B", "Barcelona", "Move", "Madrid"],["C", "London", "Move", "Madrid"]])
        self.assertEqual(v, [["A","[dead]"],["B","[dead]"],["C","[dead]"]])

    def test_eval_5(self):
        v = diplomacy_eval([["A","Madrid","Hold"],["B", "Barcelona", "Move", "Madrid"],["C", "London", "Move", "Madrid"],["D", "Paris", "Support", "B"]])
        self.assertEqual(v, [["A","[dead]"],["B","Madrid"],["C","[dead]"],["D","Paris"]])

    def test_eval_6(self):
        v = diplomacy_eval([["A","Madrid","Hold"],["B", "Barcelona", "Move", "Madrid"],["C", "London", "Move", "Madrid"],["D", "Paris", "Support", "B"],["E", "Austin", "Support", "A"]])
        self.assertEqual(v, [["A","[dead]"],["B","[dead]"],["C","[dead]"],["D","Paris"],["E","Austin"]])

    def test_eval_7(self):
        v = diplomacy_eval([["A","Madrid","Hold"]])
        self.assertEqual(v, [["A","Madrid"]])

    def test_eval_8(self):
        v = diplomacy_eval([["A","Madrid", "Move", "London"],["B", "Barcelona", "Move", "Madrid"]])
        self.assertEqual(v, [["A", "London"],["B", "Madrid"]])

    def test_eval_9(self):
        v = diplomacy_eval([["A","Madrid", "Move", "London"],["B", "London", "Hold"]])
        self.assertEqual(v, [["A","[dead]"],["B","[dead]"]])

    def test_eval_10(self):
        v = diplomacy_eval([["A","Madrid","Move","London"],["B", "London", "Support", "C"]])
        self.assertEqual(v, [["A", "[dead]"],["B", "[dead]"]])

    def test_eval_11(self):
        v = diplomacy_eval([["A","Paris", "Move", "Madrid"],["B", "Barcelona", "Support", "D"],["C", "London", "Support", "D"],["D", "Madrid","Hold"]])
        self.assertEqual(v, [["A","[dead]"],["B", "Barcelona"],["C", "London"],["D", "Madrid"]])

    def test_eval_12(self):
        v = diplomacy_eval([["A","Madrid","Hold" ],["B", "Barcelona", "Support", "A"],["C", "London", "Support", "A"],["D","Paris", "Move", "Madrid" ]])
        self.assertEqual(v, [["A","Madrid"],["B", "Barcelona"],["C", "London"],["D", "[dead]"]])

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, [["A","[dead]"],["B","Madrid"],["C","[dead]"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [["A","[dead]"],["B","Madrid"],["C","[dead]"],["D","Paris"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [["A","[dead]"],["B", "Madrid"],["C","London"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support A\nD Paris Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC London\nD [dead]\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Move London\nB London Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")


    def test_solve_7(self):
        r = StringIO("A Madrid Support C\nB Barcelona Hold\nC London Hold\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC London\nD [dead]\n")




# ----
# main
# ----
if __name__ == "__main__":
    main()
""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.........................
----------------------------------------------------------------------
Ran 25 tests in 0.002s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          73      0     54      0   100%
TestDiplomacy.py     101      0      0      0   100%
--------------------------------------------------------------
TOTAL                174      0     54      0   100%

"""
